// A magic square of order n is an arrangement of n × n numbers, usually distinct integers, in a
// square, such that the numbers in all rows, all columns, and both diagonals sum to the same constant.
//
// .txt files copied from https://en.wikipedia.org/wiki/Magic_square

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
//import java.lang.String.*;

public class MagicSquares {

    public static void main(String[] args) throws IOException{

        int rowSum, mainDiagSum, secondDiagSum;

// Path names are relative to project directory (Eclipse, Idea Quirk )
        String[] file = {"./src/Luna.txt", "./src/Mercury.txt", "./src/OrionBitMassive.txt"};

        for ( int k=0; k < file.length; k++ ) {
            mainDiagSum = 0;
            secondDiagSum = 0;
            FileReader fr0 = new FileReader(file[k]);
            BufferedReader br0 = new BufferedReader(fr0);

            int numsInRow = br0.readLine().split("\t").length;       // # nums in a row as well as in a column
            br0.close();

            int[] colSums = new int[numsInRow];                            // column Sums

            FileReader fr = new FileReader(file[k]);
            BufferedReader br = new BufferedReader(fr);

            String line = null;

            String[] strLine = new String[numsInRow];                              // array of strings in row; like "12 ", "3 ", ...

            int iRow = 0;
            while ((line = br.readLine()) != null) {

                System.out.print(line);

                strLine = line.split("\t");
                rowSum = 0;

                for ( int jCol=0; jCol < numsInRow; jCol++ ) {
                    Integer currentNum = Integer.parseInt(strLine[jCol].trim());   // trimming whitespace - "12 " doesn't parse to int
                    rowSum += currentNum;
                    colSums[jCol] += currentNum;
                    if ( iRow == jCol ) {
                        mainDiagSum += currentNum;
                    }
                    if ( (numsInRow - 1 - jCol) == iRow ) {
                        secondDiagSum += currentNum;

                    }
                }
                System.out.println(",\trow " + iRow + " sum: " + rowSum);          // sum of numbers in a row
                iRow += 1;
            }

            for ( int j=0; j < numsInRow; j++ ) {
                System.out.println("column " + j + " sum: " + colSums[j]);
            }
            System.out.println("sum of elements on main diagonal: " + mainDiagSum);
            System.out.println("sum of elements on second diagonal: " + secondDiagSum);
            br.close();
            System.out.println();
        }
    }
}

